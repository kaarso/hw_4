import java.util.Objects;


/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction yks = new Lfraction(4,10);
      Lfraction kaks = new Lfraction(3,10);
      //System.out.println(yks.plus(kaks).toString());
      String test = "27";
      System.out.println(Lfraction.valueOf(test));


   }

   private long lug;
   private long nim;

   public Lfraction (long a, long b) {
       if (this == null) throw new RuntimeException("ei taha nulli");
      if (b == 0) throw new IllegalArgumentException("nulliga ei saa jagada!");
      lug = a;
      nim = b;
      if (nim < 0) {
         lug = -lug;
         nim = -nim;
      }

      long yhistegur = syt(lug, nim);
      if (yhistegur < 0) yhistegur = - yhistegur;
      lug = lug / yhistegur;
      nim = nim / yhistegur;

   }


   public long getNumerator() {
      return lug;
   }

   public long getDenominator() { 
      return nim;
   }

   @Override
   public String toString() {
      return Long.toString(lug) + "/" + Long.toString(nim);
   }

   @Override
   public boolean equals (Object m) {
      if (((Lfraction)m).getNumerator() == this.getNumerator() &&
              ((Lfraction)m).getDenominator() == this.getDenominator() ) {
         return true;
      } else return false;
   }

   @Override
   public int hashCode() {
       if (this == null) {
           throw new RuntimeException("Tühjusest (null) ei saa hashcode võtta!");
       }
       try {
           long a = this.getNumerator();
           long b = this.getDenominator();
           //System.out.println((a + b));
           long c = a + b * (a % b);
           int hash = Objects.hashCode(c);
           return hash;
       }
       catch(NullPointerException e) {
           throw new RuntimeException("Tekkis viga hashcode arvutamisel");
       }
   }

   public Lfraction plus (Lfraction m) {
       long lug1 = this.getNumerator();
       long nim1 = this.getDenominator();
       long lug2 = m.getNumerator();
       long nim2 = m.getDenominator();
       long uusNim = nim1 * nim2;
       long uusLug = lug1 * nim2 + lug2 * nim1;
       Lfraction uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   public Lfraction times (Lfraction m) {
       long lug1 = this.getNumerator();
       long nim1 = this.getDenominator();
       long lug2 = m.getNumerator();
       long nim2 = m.getDenominator();
       long uusNim = nim1 * nim2;
       long uusLug = lug1 * lug2;
       Lfraction uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   public Lfraction inverse() {
       long uusLug = this.getDenominator();
       long uusNim = this.getNumerator();
       if (uusLug == 0 || uusNim == 0) throw new RuntimeException("Nulliga ei saa jagada!");
       Lfraction uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   public Lfraction opposite() {
       long uusLug = - this.getNumerator();
       long uusNim = this.getDenominator();
       Lfraction uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   public Lfraction minus (Lfraction m) {
       Lfraction uusMurd;
       long lug1 = this.getNumerator();
       long nim1 = this.getDenominator();
       long lug2 = m.getNumerator();
       long nim2 = m.getDenominator();
       long uusNim = nim1 * nim2;
       long uusLug = lug1 * nim2 - lug2 * nim1;
       uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   public Lfraction divideBy (Lfraction m) {
       long lug1 = this.getNumerator();
       long nim1 = this.getDenominator();
       long lug2 = m.getNumerator();
       long nim2 = m.getDenominator();
       if (nim1 == 0 || lug2 == 0) throw new RuntimeException("Nulliga ei saa jagada!");
       long uusNim = nim1 * lug2;
       long uusLug = lug1 * nim2;
       Lfraction uusMurd = new Lfraction(uusLug, uusNim);
       return uusMurd;
   }

   @Override
   public int compareTo (Lfraction m) {
       long lug1 = this.getNumerator();
       long nim1 = this.getDenominator();
       long lug2 = m.getNumerator();
       long nim2 = m.getDenominator();
       if (m.equals(this))return 0;
       if (this.getNumerator() * m.getDenominator() < m.getNumerator() * this.getDenominator()) return -1;
       else return 1;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(),getDenominator());
   }

   public long integerPart() {
      return getNumerator()/getDenominator();

   }

   public Lfraction fractionPart() {
      long uusLug = getNumerator() % getDenominator();
      return new Lfraction(uusLug, getDenominator());
   }


   public double toDouble() {
       return (double)getNumerator()/getDenominator();
   }

   public static Lfraction toLfraction (double f, long d) {

      long uusLug = Math.round(f*d);
      return new Lfraction(uusLug, d);
   }

   public static Lfraction valueOf (String s) {
       char[] chars = s.toCharArray();
       if(!s.contains("/")) throw new RuntimeException("Puudub murrujoon stringis: " + s);
       int slashCount = 0;
       char current = 0;
       if (!Character.isDigit(chars[chars.length - 1])) throw new RuntimeException("Stringis " + s + ", murru viimane sümbol peab olema arv");
       for (char c : chars) {
           if (current == '-' && !Character.isDigit(c)) {
               throw new RuntimeException("Stringis " + s + ", miinusmärgile peab järgnema number");
           }
           if (c == '-') current = c;

           if (c == '/') slashCount ++;
           if (c != '-' && c != '/' && !Character.isDigit(c)) throw new RuntimeException("Sobimatu string: " + s);
       }
       if (slashCount != 1) throw new RuntimeException("Stringis " + s + " on liiga palju murrujooni");
       String[] splititud = s.split("/");
       return new Lfraction(Long.parseLong(splititud[0]),Long.parseLong(splititud[1]));
   }

   public static long syt(long a, long b) {
      if (b==0) return a;
      return syt(b,a%b);
   }

}

// viited:
// murru mõistlikule kujule viimine:
// https://et.wikipedia.org/wiki/Suurim_%C3%BChistegur
// https://stackoverflow.com/questions/25605682/correct-implementation-of-the-equals-method-for-fractions
// https://stackoverflow.com/questions/4009198/java-get-greatest-common-divisor
// https://en.wikipedia.org/wiki/Euclidean_algorithm
// http://pages.cs.wisc.edu/~hasti/cs302/examples/Parsing/parseString.html
// https://stackoverflow.com/questions/8248277/how-to-determine-if-a-string-has-non-alphanumeric-characters